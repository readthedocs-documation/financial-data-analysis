## credit-risk  
https://ibm.github.io/credit-risk-workshop-cpd/ 
### Applicant Financial Data¶
This file has the following attributes:

CUSTOMERID (hex number, used as Primary Key)
CHECKINGSTATUS  
CREDITHISTORY   
EXISTINGSAVINGS 
INSTALLMENTPLANS    
EXISTINGCREDITSCOUNT    

### Applicant Loan Data¶    
This file has the following attributes:
CUSTOMERID  
LOANDURATION    
LOANPURPOSE 
LOANAMOUNT  
INSTALLMENTPERCENT  
OTHERSONLOAN    
RISK        
### Applicant Personal Data¶    
This file has the following attributes:
CUSTOMERID  
EMPLOYMENTDURATION  
SEX 
CURRENTRESIDENCEDURATION    
OWNSPROPERTY    
AGE 
HOUSING 
JOB 
DEPENDENTS  
TELEPHONE   
FOREIGNWORKER   
FIRSTNAME   
LASTNAME    
EMAIL   
STREETADDRESS   
CITY    
STATE   
POSTALCODE  
