## jupyter-notebook-data-analysis   

https://jupyterbook.org/start/overview.html 



```bash
pip3 install jupyterlab
pip3 install notebook
pip3 install jupyterlab-fasta
pip3 install jupyterlab-geojson
pip3 install jupyterlab-katex
pip3 install jupyterlab-mathjax3
pip3 install jupyterlab-vega2
pip3 install jupyterlab-vega3
pip3 install ipympl
pip3 install jupyterlab_sql
pip3 install ghp-import
pip3 install -U jupyter-book
pip3 install jupyterlab-github

jupyter labextension list
jupyter labextension install @jupyterlab/toc
jupyter labextension install nbgather

pip3 install -U datapane
pip3 install seaborn
pip3 install reportlab
pip3 install papermill
pip3 install voila
pip3 install scikit-learn

```


```bash
pip3 install -r requirements.txt
```

`.bashrc`   
```bash
export PATH=/home/rockhu/.local/bin:$PATH
```


```bash
jupyter-book create financial-analysis-notebook
jupyter-book build financial-analysis-notebook

```

```bash
jupyter notebook
```

## pandas   




| category              | item        | link                                                                 |
|-----------------------|-------------|----------------------------------------------------------------------|
|                       |             | https://pandas.pydata.org/docs/reference/index.html                  |
| pandas                | csv read    | https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html    |
| pandas                | excel read  | https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html  |
| pandas                | excel write | https://pandas.pydata.org/docs/reference/api/pandas.ExcelWriter.html |
| charting - matplotlib | plot_types  | https://matplotlib.org/stable/plot_types/index.html                  |
|                       |             | https://seaborn.pydata.org/index.html                                |
|                       |             | https://docs.datapane.com/                                           |
|                       |             | https://papermill.readthedocs.io/en/latest/index.html                |
|                       |             | https://mybinder.org/                                                |
|                       |             | https://www.sqlite.org/datatype3.html                                |
|                       |             | https://www.datacamp.com/community/data-science-cheatsheets          |


## matplotlib   
https://matplotlib.org/     
Matplotlib: Visualization with Python   
Matplotlib is a comprehensive library for creating static, animated, and interactive visualizations in Python. Matplotlib makes easy things easy and hard things possible.  
- Create publication quality plots.
- Make interactive figures that can zoom, pan, update.
- Customize visual style and layout.
- Export to many file formats .
- Embed in JupyterLab and Graphical User Interfaces.
- Use a rich array of third-party packages built on Matplotlib.

## mybinder.org 
gitlab.com: https://gitlab.com/readthedocs-documation/financial-data-analysis
branch: master
path to a notebook file (optional): jupyter-notebook-data-analysis/financial-analysis-notebook/notebooks.ipynb