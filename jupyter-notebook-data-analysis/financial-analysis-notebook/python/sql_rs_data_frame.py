import sqlite3
import pandas as pd
import numpy as np
import datetime

def sql_result_set_data_frame(conn, sql):
     return pd.read_sql(sql, conn)