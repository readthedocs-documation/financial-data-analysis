
import sqlite3

def load_sql(path):
    sql_file = open(path, "r")
    sql = sql_file.read()
    sql_file.close()
    return sql


def connect_db(sqlite_db):
    return sqlite3.connect(sqlite_db)