def pyplot_font_size(pyplot, font_size):
    pyplot.rc('font', size=font_size)
    pyplot.rc('axes', titlesize=font_size) 
    pyplot.rc('axes', labelsize=font_size) 
    pyplot.rc('xtick', labelsize=font_size) 
    pyplot.rc('ytick', labelsize=font_size)
    pyplot.rc('legend', fontsize=font_size)