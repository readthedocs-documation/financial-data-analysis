import pandas as pd
import numpy as np

def pandas_default_options():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.precision', 2)
    pd.set_option('display.float_format', '{:,.2f}'.format)
    pd.options.display.float_format = '{:.2f}'.format

