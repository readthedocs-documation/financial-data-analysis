INSERT INTO
    GROSS_SALES (
        SEGMENT,
        COUNTRY,
        PRODUCT,
        DISCOUNT_BAND,
        UNITS_SOLD,
        MANUFACTURING_PRICE,
        SALE_PRICE,
        GROSS_SALES,
        DISCOUNTS,
        SALES,
        COGS,
        PROFIT,
        BUSINESS_DATE,
        MONTH_NUMBER,
        MONTH_NAME,
        YEAR_NUMBER
    )
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)