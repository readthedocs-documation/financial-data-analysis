## financial-data-analysis

[spring-boot-dependencies-2.6.3.pom](https://repo1.maven.org/maven2/org/springframework/boot/spring-boot-dependencies/2.6.3/spring-boot-dependencies-2.6.3.pom)
[metrics-bom-4.2.7.pom](https://repo1.maven.org/maven2/io/dropwizard/metrics/metrics-bom/4.2.7/metrics-bom-4.2.7.pom)


[spring-batch-excel](https://github.com/spring-projects/spring-batch-extensions/tree/main/spring-batch-excel)

```bash
mkdir -p src/test/resources

cd src/main/java/com/financial/analysis
mkdir -p reader writer processor tasklet domain repository
```

```bash
mvn sortpom:sort
mvn enforcer:enforce
mvn dependency:tree > tree.log
mvn dependency:copy-dependencies
mvn dependency:analyze
mvn spotless:apply

mvn site
mvn spotbugs:gui
```


```bash
jdbc:h2:mem:financial-data-analysis
jdbc:h2:tcp://localhost:9090/mem:financial-data-analysis
```



```sql
INSERT INTO GROSS_SALES
(SEGMENT,COUNTRY,PRODUCT,DISCOUNT_BAND,UNITS_SOLD,MANUFACTURING_PRICE,SALE_PRICE,GROSS_SALES,DISCOUNTS,SALES,COGS,PROFIT,BUSINESS_DATE,MONTH_NUMBER,MONTH_NAME,YEAR_NUMBER
VALUES
(:segment,:country,:product,:discountBand,:unitsSold,:manufacturingPrice,:salePrice,:grossSales,:discounts,:sales,:cogs,:profit,:businessDate,:monthNumber,:monthName,:yearNumber)
```



-javaagent:/Applications/SpringToolSuite4.app/Contents/Eclipse/lombok.jar
