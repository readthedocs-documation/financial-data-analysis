package com.financial.analysis.repository.impl;

import com.financial.analysis.repository.GrossSaleRepository;
import javax.sql.DataSource;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

@Repository
public class GrossSaleRepositoryImpl implements GrossSaleRepository, InitializingBean {

	@Autowired
	DataSource dataSource;
	private SimpleJdbcInsert jdbcInsert;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcInsert = new SimpleJdbcInsert(dataSource).withTableName(GrossSaleRepository.TABLE_NAME);
	}
}
