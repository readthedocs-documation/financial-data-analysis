package com.financial.analysis.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GrossSale implements Serializable {
	// :segment,:country,:product,:discountBand,:unitsSold,:manufacturingPrice,:salePrice,:grossSales,:discounts,:sales,:cogs,:profit,:businessDate,:monthNumber,:monthName,:yearNumber
	private String segment;
	private String country;
	private String product;
	private String discountBand;
	private BigDecimal unitsSold;
	private BigDecimal manufacturingPrice;
	private BigDecimal salePrice;
	private BigDecimal grossSales;
	private BigDecimal discounts;
	private BigDecimal sales;
	private BigDecimal cogs;
	private BigDecimal profit;
	private Date businessDate;
	private int monthNumber;
	private String monthName;
	private int yearNumber;

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDiscountBand() {
		return discountBand;
	}

	public void setDiscountBand(String discountBand) {
		this.discountBand = discountBand;
	}

	public BigDecimal getUnitsSold() {
		return unitsSold;
	}

	public void setUnitsSold(BigDecimal unitsSold) {
		this.unitsSold = unitsSold;
	}

	public BigDecimal getManufacturingPrice() {
		return manufacturingPrice;
	}

	public void setManufacturingPrice(BigDecimal manufacturingPrice) {
		this.manufacturingPrice = manufacturingPrice;
	}

	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public BigDecimal getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(BigDecimal grossSales) {
		this.grossSales = grossSales;
	}

	public BigDecimal getDiscounts() {
		return discounts;
	}

	public void setDiscounts(BigDecimal discounts) {
		this.discounts = discounts;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getCogs() {
		return cogs;
	}

	public void setCogs(BigDecimal cogs) {
		this.cogs = cogs;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public int getMonthNumber() {
		return monthNumber;
	}

	public void setMonthNumber(int monthNumber) {
		this.monthNumber = monthNumber;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public int getYearNumber() {
		return yearNumber;
	}

	public void setYearNumber(int yearNumber) {
		this.yearNumber = yearNumber;
	}

}
