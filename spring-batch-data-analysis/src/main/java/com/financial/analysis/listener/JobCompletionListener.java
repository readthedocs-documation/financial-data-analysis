package com.financial.analysis.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

@Log4j2
public class JobCompletionListener extends JobExecutionListenerSupport {
	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(JobCompletionListener.class);

	@Override
	public void afterJob(JobExecution jobExecution) {
		log.debug("status:{}, exitStatus:{}", jobExecution.getStatus(), jobExecution.getExitStatus());
		super.afterJob(jobExecution);
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		log.debug("startTime:{}", jobExecution.getStartTime());
		super.beforeJob(jobExecution);
	}

}
