package com.financial.analysis.config;

import com.financial.analysis.listener.JobCompletionListener;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;

@Configuration
@EnableIntegration
public class SpringIntegrationConfig {

	@Bean
	public JobExecutionListener jobExecutionListener() {
		JobExecutionListener jobExecutionListener = new JobCompletionListener();
		return jobExecutionListener;
	}
}
