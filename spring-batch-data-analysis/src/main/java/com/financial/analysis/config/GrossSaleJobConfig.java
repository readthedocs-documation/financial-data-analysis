package com.financial.analysis.config;

import com.financial.analysis.domain.GrossSale;
import com.financial.analysis.mapper.GrossSaleRowMapper;
import javax.sql.DataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.extensions.excel.poi.PoiItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Log4j2
@Configuration
public class GrossSaleJobConfig {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(GrossSaleJobConfig.class);
	@Autowired
	JobBuilderFactory jobBuilderFactory;

	@Autowired
	StepBuilderFactory stepBuilderFactory;
	@Autowired
	JobExecutionListener jobExecutionListener;

	@Value("classpath:financial-*.xlsx")
	Resource[] excels;

	private static final String SQL = "INSERT INTO GROSS_SALES"
			+ "(SEGMENT,COUNTRY,PRODUCT,DISCOUNT_BAND,UNITS_SOLD,MANUFACTURING_PRICE,SALE_PRICE,GROSS_SALES,DISCOUNTS,SALES,COGS,PROFIT,BUSINESS_DATE,MONTH_NUMBER,MONTH_NAME,YEAR_NUMBER)"
			+ " VALUES "
			+ "(:segment,:country,:product,:discountBand,:unitsSold,:manufacturingPrice,:salePrice,:grossSales,:discounts,:sales,:cogs,:profit,:businessDate,:monthNumber,:monthName,:yearNumber)";

	@Bean
	public JobParameters jobParameters() {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addString("excel", "financial.xlsx");
		return builder.toJobParameters();
	}

	@Bean
	public MultiResourceItemReader<GrossSale> grossSaleMultiResourceItemReader(PoiItemReader<GrossSale> grossSaleReader) {
		MultiResourceItemReader<GrossSale> reader = new MultiResourceItemReader<>();
		reader.setResources(excels);
		reader.setDelegate(grossSaleReader);
		return reader;
	}

	// "financial.xlsx"
	@Bean
	public PoiItemReader<GrossSale> grossSaleReader(GrossSaleRowMapper grossSaleRowMapper) {
		PoiItemReader<GrossSale> reader = new PoiItemReader<GrossSale>();
		reader.setLinesToSkip(1);
		reader.setResource(new ClassPathResource("financial.xlsx"));
		reader.setRowMapper(grossSaleRowMapper);
		reader.setSkippedRowsCallback((row) -> {
			log.debug("skip row: {}, content:[{}]", row.getCurrentRowIndex(), row.getCurrentRow());

		});
		reader.setStrict(true);
		return reader;
	}

	@Bean
	public GrossSaleRowMapper grossSaleRowMapper() throws Exception {
		return new GrossSaleRowMapper();
	}

	@Bean
	public JdbcBatchItemWriter<GrossSale> grossSaleWriter(DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<GrossSale>().itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<GrossSale>()).sql(SQL)
				.dataSource(dataSource).build();
	}

	@Bean
	public Job grossSaleImportJob(Step grossSaleImportStep) {
		return jobBuilderFactory.get("grossSaleImportJob").listener(jobExecutionListener).incrementer(new RunIdIncrementer()).flow(grossSaleImportStep).end()
				.build();
	}

	@Bean
	public Step grossSaleImportStep(MultiResourceItemReader<GrossSale> grossSaleMultiResourceItemReader, ItemWriter<GrossSale> grossSaleWriter) {
		return stepBuilderFactory.get("grossSaleImportStep").<GrossSale, GrossSale>chunk(100).reader(grossSaleMultiResourceItemReader).writer(grossSaleWriter)
				.build();
	}
}
