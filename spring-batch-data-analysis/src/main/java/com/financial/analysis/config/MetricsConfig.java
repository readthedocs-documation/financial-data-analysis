package com.financial.analysis.config;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfig {
	@Bean
	public MetricRegistry metricRegistry() {
		final MetricRegistry metricRegistry = new MetricRegistry();
		metricRegistry.register("jvm.memory", new MemoryUsageGaugeSet());
		metricRegistry.register("jvm.thread-states", new ThreadStatesGaugeSet());
		metricRegistry.register("jvm.garbage-collector", new GarbageCollectorMetricSet());
		return metricRegistry;
	}
}
