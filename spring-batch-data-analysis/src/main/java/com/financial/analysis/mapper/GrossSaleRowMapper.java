package com.financial.analysis.mapper;

import com.financial.analysis.domain.GrossSale;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.extensions.excel.RowMapper;
import org.springframework.batch.extensions.excel.support.rowset.RowSet;

public class GrossSaleRowMapper implements RowMapper<GrossSale> {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(GrossSaleRowMapper.class);

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	public static final String BIG_DECIMAL_DEFAULT_VALUE = "0.00";

	@Override
	public GrossSale mapRow(RowSet rs) throws Exception {
		GrossSale item = new GrossSale();

		// :segment,:country,:product,:discountBand,:unitsSold,
		// :manufacturingPrice,:salePrice,:grossSales,:discounts,:sales,
		// :cogs,:profit,:businessDate,:monthNumber,:monthName,
		// :yearNumber
		if (rs == null || ArrayUtils.isEmpty(rs.getCurrentRow())) {
			return null;
		}

		String[] values = rs.getCurrentRow();

		log.trace("row:{} values:{}", rs.getCurrentRowIndex(), StringUtils.join(values, ","));

		item.setSegment(values[0]);
		item.setCountry(values[1]);
		item.setProduct(values[2]);
		item.setDiscountBand(values[3]);
		item.setUnitsSold(bigDecimal(values[4], BIG_DECIMAL_DEFAULT_VALUE));

		item.setManufacturingPrice(bigDecimal(values[5], BIG_DECIMAL_DEFAULT_VALUE));
		item.setSalePrice(bigDecimal(values[6], BIG_DECIMAL_DEFAULT_VALUE));
		item.setGrossSales(bigDecimal(values[7], BIG_DECIMAL_DEFAULT_VALUE));
		item.setDiscounts(bigDecimal(values[8], BIG_DECIMAL_DEFAULT_VALUE));
		item.setSales(bigDecimal(values[9], BIG_DECIMAL_DEFAULT_VALUE));

		item.setCogs(bigDecimal(values[10], BIG_DECIMAL_DEFAULT_VALUE));
		item.setProfit(bigDecimal(values[11], BIG_DECIMAL_DEFAULT_VALUE));
		item.setBusinessDate(sdf.parse(values[12]));
		item.setMonthNumber(Integer.valueOf(values[13]));
		item.setMonthName(values[14]);

		item.setYearNumber(Integer.valueOf(values[15]));

		return item;
	}

	private BigDecimal bigDecimal(String value, String defaultValue) {
		return new BigDecimal(StringUtils.defaultString(value, defaultValue));
	}

}
