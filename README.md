## data analysis

http://optimumsportsperformance.com/blog/data-analysis-template-in-r-markdown-jupyter-notebook/
Cross Industry Standard Process for Data Mining (CRISP-DM) and the Problem, Plan, Data, Analysis, and Conclusion (PPDAC) Cycle.

http://optimumsportsperformance.com/blog/data-analysis-template-in-r-markdown-jupyter-notebook/

![Cross Industry Standard Process for Data Mining](images/CRSIP-DM.png)

![Problem, Plan, Data, Analysis, and Conclusion](images/PPDAC.png)

### apache derby data set import

```bash

```

### dbeaver community edition

https://dbeaver.io/download/

```sql
SELECT COUNTRY  , SUM(GROSS_SALES) AS TOTAL_GROSS_SALES  , YEAR_NUMBER , MONTH_NUMBER  FROM GROSS_SALES
GROUP BY
YEAR_NUMBER , MONTH_NUMBER , COUNTRY
ORDER BY YEAR_NUMBER , MONTH_NUMBER , TOTAL_GROSS_SALES DESC , COUNTRY
;
```

<details>
    <summary>result</summary>   

| COUNTRY                  | TOTAL_GROSS_SALES | YEAR_NUMBER | MONTH_NUMBER |
|--------------------------|-------------------|-------------|--------------|
| Germany                  | 1152283.00        | 2013        | 9            |
| Canada                   | 1044495.00        | 2013        | 9            |
| United States of America | 998013.00         | 2013        | 9            |
| France                   | 857285.00         | 2013        | 9            |
| Mexico                   | 677660.00         | 2013        | 9            |
| Germany                  | 2619490.00        | 2013        | 10           |
| France                   | 2545484.00        | 2013        | 10           |
| United States of America | 2133068.00        | 2013        | 10           |
| Canada                   | 1310190.00        | 2013        | 10           |
| Mexico                   | 1220456.00        | 2013        | 10           |
| Germany                  | 1913302.00        | 2013        | 11           |
| Mexico                   | 1844458.00        | 2013        | 11           |
| United States of America | 1776455.00        | 2013        | 11           |
| Canada                   | 1517608.00        | 2013        | 11           |
| France                   | 1115515.00        | 2013        | 11           |
| Canada                   | 1683093.00        | 2013        | 12           |
| Mexico                   | 1408327.00        | 2013        | 12           |
| France                   | 1062599.00        | 2013        | 12           |
| Germany                  | 945190.00         | 2013        | 12           |
| United States of America | 735816.00         | 2013        | 12           |
| Mexico                   | 1818726.00        | 2014        | 1            |
| France                   | 1622178.00        | 2014        | 1            |
| United States of America | 1528966.50        | 2014        | 1            |
| Canada                   | 1355721.00        | 2014        | 1            |
| Germany                  | 981812.00         | 2014        | 1            |
| Mexico                   | 1711835.00        | 2014        | 2            |
| Canada                   | 1571245.00        | 2014        | 2            |
| France                   | 1560631.00        | 2014        | 2            |
| Germany                  | 1456503.00        | 2014        | 2            |
| United States of America | 1398987.00        | 2014        | 2            |
| United States of America | 1946498.00        | 2014        | 3            |
| France                   | 1777761.00        | 2014        | 3            |
| Mexico                   | 975021.00         | 2014        | 3            |
| Canada                   | 905723.00         | 2014        | 3            |
| Germany                  | 519023.00         | 2014        | 3            |
| United States of America | 1716141.00        | 2014        | 4            |
| Canada                   | 1712314.50        | 2014        | 4            |
| Germany                  | 1473556.50        | 2014        | 4            |
| France                   | 1436293.50        | 2014        | 4            |
| Mexico                   | 1091087.00        | 2014        | 4            |
| United States of America | 2125121.00        | 2014        | 5            |
| Germany                  | 1368335.00        | 2014        | 5            |
| Mexico                   | 1296696.00        | 2014        | 5            |
| France                   | 1128877.00        | 2014        | 5            |
| Canada                   | 848882.00         | 2014        | 5            |
| Canada                   | 3084512.00        | 2014        | 6            |
| Mexico                   | 2412788.00        | 2014        | 6            |
| France                   | 1716334.00        | 2014        | 6            |
| Germany                  | 1661192.00        | 2014        | 6            |
| United States of America | 1394146.00        | 2014        | 6            |
| United States of America | 2489734.50        | 2014        | 7            |
| Canada                   | 2303625.00        | 2014        | 7            |
| Germany                  | 1776361.00        | 2014        | 7            |
| France                   | 1233534.00        | 2014        | 7            |
| Mexico                   | 1029773.00        | 2014        | 7            |
| United States of America | 2217423.00        | 2014        | 8            |
| Germany                  | 1139514.00        | 2014        | 8            |
| Mexico                   | 1116197.00        | 2014        | 8            |
| Canada                   | 992458.00         | 2014        | 8            |
| France                   | 860367.00         | 2014        | 8            |
| France                   | 1840635.00        | 2014        | 9            |
| United States of America | 1571735.00        | 2014        | 9            |
| Germany                  | 1307992.00        | 2014        | 9            |
| Mexico                   | 1101624.00        | 2014        | 9            |
| Canada                   | 1023331.00        | 2014        | 9            |
| France                   | 3616266.00        | 2014        | 10           |
| Germany                  | 3559008.00        | 2014        | 10           |
| Canada                   | 2454416.00        | 2014        | 10           |
| Mexico                   | 1989116.00        | 2014        | 10           |
| United States of America | 1694618.00        | 2014        | 10           |
| United States of America | 1673500.00        | 2014        | 11           |
| Mexico                   | 1266931.00        | 2014        | 11           |
| France                   | 1231847.00        | 2014        | 11           |
| Canada                   | 1074128.00        | 2014        | 11           |
| Germany                  | 701504.00         | 2014        | 11           |
| Canada                   | 4050422.00        | 2014        | 12           |
| France                   | 2476068.00        | 2014        | 12           |
| Germany                  | 2346402.00        | 2014        | 12           |
| United States of America | 1869136.00        | 2014        | 12           |
| Mexico                   | 1766240.00        | 2014        | 12           |
</details>
```sql
SELECT COUNTRY , SEGMENT , SUM(GROSS_SALES) AS TOTAL_GROSS_SALES  , YEAR_NUMBER , MONTH_NUMBER  FROM GROSS_SALES
GROUP BY
YEAR_NUMBER , MONTH_NUMBER , COUNTRY , SEGMENT
ORDER BY YEAR_NUMBER , MONTH_NUMBER , COUNTRY ,SEGMENT
;
```
<details>
    <summary>result</summary>   
｜ COUNTRY | SEGMENT | TOTAL_GROSS_SALES | YEAR_NUMBER | MONTH_NUMBER |
｜ --- | --- | --- | --- | --- |
｜ Canada |Channel Partners| 33132.00| 2013| 9|
｜ Canada |Enterprise | 302000.00| 2013| 9|
｜ Canada |Government | 287413.00| 2013| 9|
｜ Canada |Midmarket | 37050.00| 2013| 9|
｜ Canada |Small Business | 384900.00| 2013| 9|
｜ France |Channel Partners| 26808.00| 2013| 9|
｜ France |Enterprise | 127875.00| 2013| 9|
｜ France |Government | 597767.00| 2013| 9|
｜ France |Midmarket | 8235.00| 2013| 9|
｜ France |Small Business | 96600.00| 2013| 9|
｜ Germany |Channel Partners| 26580.00| 2013| 9|
｜ Germany |Enterprise | 124250.00| 2013| 9|
｜ Germany |Government | 654653.00| 2013| 9|
｜ Germany |Midmarket | 9900.00| 2013| 9|
｜ Germany |Small Business | 336900.00| 2013| 9|
｜ Mexico |Channel Partners| 12060.00| 2013| 9|
｜ Mexico |Enterprise | 118375.00| 2013| 9|
｜ Mexico |Government | 301000.00| 2013| 9|
｜ Mexico |Midmarket | 9825.00| 2013| 9|
｜ Mexico |Small Business | 236400.00| 2013| 9|
｜ United States of America|Channel Partners| 23868.00| 2013| 9|
｜ United States of America|Enterprise | 41250.00| 2013| 9|
｜ United States of America|Government | 139530.00| 2013| 9|
｜ United States of America|Midmarket | 43965.00| 2013| 9|
｜ United States of America|Small Business | 749400.00| 2013| 9|
｜ Canada |Channel Partners| 55176.00| 2013| 10|
｜ Canada |Enterprise | 86250.00| 2013| 10|
｜ Canada |Government | 955074.00| 2013| 10|
｜ Canada |Midmarket | 52290.00| 2013| 10|
｜ Canada |Small Business | 161400.00| 2013| 10|
｜ France |Channel Partners| 28752.00| 2013| 10|
｜ France |Enterprise | 176000.00| 2013| 10|
｜ France |Government | 1543122.00| 2013| 10|
｜ France |Midmarket | 65010.00| 2013| 10|
｜ France |Small Business | 732600.00| 2013| 10|
｜ Germany |Channel Partners| 18384.00| 2013| 10|
｜ Germany |Enterprise | 202250.00| 2013| 10|
｜ Germany |Government | 2212106.00| 2013| 10|
｜ Germany |Midmarket | 58350.00| 2013| 10|
｜ Germany |Small Business | 128400.00| 2013| 10|
｜ Mexico |Channel Partners| 8808.00| 2013| 10|
｜ Mexico |Enterprise | 536250.00| 2013| 10|
｜ Mexico |Government | 333578.00| 2013| 10|
｜ Mexico |Midmarket | 45420.00| 2013| 10|
｜ Mexico |Small Business | 296400.00| 2013| 10|
｜ United States of America|Channel Partners| 9264.00| 2013| 10|
｜ United States of America|Enterprise | 165750.00| 2013| 10|
｜ United States of America|Government | 561524.00| 2013| 10|
｜ United States of America|Midmarket | 20130.00| 2013| 10|
｜ United States of America|Small Business | 1376400.00| 2013| 10|
｜ Canada |Channel Partners| 26664.00| 2013| 11|
｜ Canada |Enterprise | 369250.00| 2013| 11|
｜ Canada |Government | 677094.00| 2013| 11|
｜ Canada |Midmarket | 23400.00| 2013| 11|
｜ Canada |Small Business | 421200.00| 2013| 11|
｜ France |Channel Partners| 21420.00| 2013| 11|
｜ France |Enterprise | 232125.00| 2013| 11|
｜ France |Government | 741355.00| 2013| 11|
｜ France |Midmarket | 4815.00| 2013| 11|
｜ France |Small Business | 115800.00| 2013| 11|
｜ Germany |Channel Partners| 21300.00| 2013| 11|
｜ Germany |Enterprise | 312500.00| 2013| 11|
｜ Germany |Government | 804152.00| 2013| 11|
｜ Germany |Midmarket | 14550.00| 2013| 11|
｜ Germany |Small Business | 760800.00| 2013| 11|
｜ Mexico |Channel Partners| 33156.00| 2013| 11|
｜ Mexico |Enterprise | 207500.00| 2013| 11|
｜ Mexico |Government | 794252.00| 2013| 11|
｜ Mexico |Midmarket | 28050.00| 2013| 11|
｜ Mexico |Small Business | 781500.00| 2013| 11|
｜ United States of America|Channel Partners| 10944.00| 2013| 11|
｜ United States of America|Enterprise | 225500.00| 2013| 11|
｜ United States of America|Government | 729591.00| 2013| 11|
｜ United States of America|Midmarket | 38220.00| 2013| 11|
｜ United States of America|Small Business | 772200.00| 2013| 11|
｜ Canada |Channel Partners| 10896.00| 2013| 12|
｜ Canada |Enterprise | 239500.00| 2013| 12|
｜ Canada |Government | 669617.00| 2013| 12|
｜ Canada |Midmarket | 32280.00| 2013| 12|
｜ Canada |Small Business | 730800.00| 2013| 12|
｜ France |Channel Partners| 3672.00| 2013| 12|
｜ France |Enterprise | 185250.00| 2013| 12|
｜ France |Government | 483962.00| 2013| 12|
｜ France |Midmarket | 33915.00| 2013| 12|
｜ France |Small Business | 355800.00| 2013| 12|
｜ Germany |Channel Partners| 21240.00| 2013| 12|
｜ Germany |Enterprise | 110875.00| 2013| 12|
｜ Germany |Government | 681135.00| 2013| 12|
｜ Germany |Midmarket | 31740.00| 2013| 12|
｜ Germany |Small Business | 100200.00| 2013| 12|
｜ Mexico |Channel Partners| 16500.00| 2013| 12|
｜ Mexico |Enterprise | 352625.00| 2013| 12|
｜ Mexico |Government | 703502.00| 2013| 12|
｜ Mexico |Midmarket | 5700.00| 2013| 12|
｜ Mexico |Small Business | 330000.00| 2013| 12|
｜ United States of America|Channel Partners| 24180.00| 2013| 12|
｜ United States of America|Enterprise | 304750.00| 2013| 12|
｜ United States of America|Government | 135611.00| 2013| 12|
｜ United States of America|Midmarket | 28875.00| 2013| 12|
｜ United States of America|Small Business | 242400.00| 2013| 12|
｜ Canada |Channel Partners| 38934.00| 2014| 1|
｜ Canada |Enterprise | 207375.00| 2014| 1|
｜ Canada |Government | 841752.00| 2014| 1|
｜ Canada |Midmarket | 5760.00| 2014| 1|
｜ Canada |Small Business | 261900.00| 2014| 1|
｜ France |Channel Partners| 20808.00| 2014| 1|
｜ France |Enterprise | 248437.50| 2014| 1|
｜ France |Government | 562620.00| 2014| 1|
｜ France |Midmarket | 59962.50| 2014| 1|
｜ France |Small Business | 730350.00| 2014| 1|
｜ Germany |Channel Partners| 29748.00| 2014| 1|
｜ Germany |Enterprise | 395625.00| 2014| 1|
｜ Germany |Government | 304124.00| 2014| 1|
｜ Germany |Midmarket | 10215.00| 2014| 1|
｜ Germany |Small Business | 242100.00| 2014| 1|
｜ Mexico |Channel Partners| 28080.00| 2014| 1|
｜ Mexico |Enterprise | 69250.00| 2014| 1|
｜ Mexico |Government | 908981.00| 2014| 1|
｜ Mexico |Midmarket | 42915.00| 2014| 1|
｜ Mexico |Small Business | 769500.00| 2014| 1|
｜ United States of America|Channel Partners| 23472.00| 2014| 1|
｜ United States of America|Enterprise | 72375.00| 2014| 1|
｜ United States of America|Government | 376294.50| 2014| 1|
｜ United States of America|Midmarket | 8325.00| 2014| 1|
｜ United States of America|Small Business | 1048500.00| 2014| 1|
｜ Canada |Channel Partners| 23244.00| 2014| 2|
｜ Canada |Enterprise | 119000.00| 2014| 2|
｜ Canada |Government | 793256.00| 2014| 2|
｜ Canada |Midmarket | 35445.00| 2014| 2|
｜ Canada |Small Business | 600300.00| 2014| 2|
｜ France |Channel Partners| 22380.00| 2014| 2|
｜ France |Enterprise | 225500.00| 2014| 2|
｜ France |Government | 990611.00| 2014| 2|
｜ France |Midmarket | 34440.00| 2014| 2|
｜ France |Small Business | 287700.00| 2014| 2|
｜ Germany |Channel Partners| 13392.00| 2014| 2|
｜ Germany |Enterprise | 100875.00| 2014| 2|
｜ Germany |Government | 540366.00| 2014| 2|
｜ Germany |Midmarket | 4170.00| 2014| 2|
｜ Germany |Small Business | 797700.00| 2014| 2|
｜ Mexico |Channel Partners| 8724.00| 2014| 2|
｜ Mexico |Enterprise | 196875.00| 2014| 2|
｜ Mexico |Government | 667526.00| 2014| 2|
｜ Mexico |Midmarket | 14610.00| 2014| 2|
｜ Mexico |Small Business | 824100.00| 2014| 2|
｜ United States of America|Channel Partners| 22296.00| 2014| 2|
｜ United States of America|Enterprise | 344375.00| 2014| 2|
｜ United States of America|Government | 156406.00| 2014| 2|
｜ United States of America|Midmarket | 22710.00| 2014| 2|
｜ United States of America|Small Business | 853200.00| 2014| 2|
｜ Canada |Channel Partners| 7176.00| 2014| 3|
｜ Canada |Enterprise | 221750.00| 2014| 3|
｜ Canada |Government | 380892.00| 2014| 3|
｜ Canada |Midmarket | 29505.00| 2014| 3|
｜ Canada |Small Business | 266400.00| 2014| 3|
｜ France |Channel Partners| 23604.00| 2014| 3|
｜ France |Enterprise | 298125.00| 2014| 3|
｜ France |Government | 676017.00| 2014| 3|
｜ France |Midmarket | 37515.00| 2014| 3|
｜ France |Small Business | 742500.00| 2014| 3|
｜ Germany |Channel Partners| 25932.00| 2014| 3|
｜ Germany |Enterprise | 99375.00| 2014| 3|
｜ Germany |Government | 302201.00| 2014| 3|
｜ Germany |Midmarket | 13815.00| 2014| 3|
｜ Germany |Small Business | 77700.00| 2014| 3|
｜ Mexico |Channel Partners| 6000.00| 2014| 3|
｜ Mexico |Enterprise | 139250.00| 2014| 3|
｜ Mexico |Government | 466261.00| 2014| 3|
｜ Mexico |Midmarket | 33210.00| 2014| 3|
｜ Mexico |Small Business | 330300.00| 2014| 3|
｜ United States of America|Channel Partners| 17580.00| 2014| 3|
｜ United States of America|Enterprise | 374000.00| 2014| 3|
｜ United States of America|Government | 646863.00| 2014| 3|
｜ United States of America|Midmarket | 10155.00| 2014| 3|
｜ United States of America|Small Business | 897900.00| 2014| 3|
｜ Canada |Channel Partners| 42246.00| 2014| 4|
｜ Canada |Enterprise | 92812.50| 2014| 4|
｜ Canada |Government | 412296.00| 2014| 4|
｜ Canada |Midmarket | 24210.00| 2014| 4|
｜ Canada |Small Business | 1140750.00| 2014| 4|
｜ France |Channel Partners| 20862.00| 2014| 4|
｜ France |Enterprise | 530437.50| 2014| 4|
｜ France |Government | 296079.00| 2014| 4|
｜ France |Midmarket | 57015.00| 2014| 4|
｜ France |Small Business | 531900.00| 2014| 4|
｜ Germany |Channel Partners| 34056.00| 2014| 4|
｜ Germany |Enterprise | 527437.50| 2014| 4|
｜ Germany |Government | 480063.00| 2014| 4|
｜ Germany |Midmarket | 7650.00| 2014| 4|
｜ Germany |Small Business | 424350.00| 2014| 4|
｜ Mexico |Channel Partners| 22992.00| 2014| 4|
｜ Mexico |Enterprise | 134250.00| 2014| 4|
｜ Mexico |Government | 412325.00| 2014| 4|
｜ Mexico |Midmarket | 39420.00| 2014| 4|
｜ Mexico |Small Business | 482100.00| 2014| 4|
｜ United States of America|Channel Partners| 23436.00| 2014| 4|
｜ United States of America|Enterprise | 430687.50| 2014| 4|
｜ United States of America|Government | 516592.50| 2014| 4|
｜ United States of America|Midmarket | 55125.00| 2014| 4|
｜ United States of America|Small Business | 690300.00| 2014| 4|
｜ Canada |Channel Partners| 25308.00| 2014| 5|
｜ Canada |Enterprise | 205625.00| 2014| 5|
｜ Canada |Government | 88419.00| 2014| 5|
｜ Canada |Midmarket | 18930.00| 2014| 5|
｜ Canada |Small Business | 510600.00| 2014| 5|
｜ France |Channel Partners| 10392.00| 2014| 5|
｜ France |Enterprise | 179125.00| 2014| 5|
｜ France |Government | 621570.00| 2014| 5|
｜ France |Midmarket | 42390.00| 2014| 5|
｜ France |Small Business | 275400.00| 2014| 5|
｜ Germany |Channel Partners| 10560.00| 2014| 5|
｜ Germany |Enterprise | 284500.00| 2014| 5|
｜ Germany |Government | 531925.00| 2014| 5|
｜ Germany |Midmarket | 22950.00| 2014| 5|
｜ Germany |Small Business | 518400.00| 2014| 5|
｜ Mexico |Channel Partners| 31932.00| 2014| 5|
｜ Mexico |Enterprise | 42625.00| 2014| 5|
｜ Mexico |Government | 1041164.00| 2014| 5|
｜ Mexico |Midmarket | 3675.00| 2014| 5|
｜ Mexico |Small Business | 177300.00| 2014| 5|
｜ United States of America|Channel Partners| 21672.00| 2014| 5|
｜ United States of America|Enterprise | 355500.00| 2014| 5|
｜ United States of America|Government | 860699.00| 2014| 5|
｜ United States of America|Midmarket | 11850.00| 2014| 5|
｜ United States of America|Small Business | 875400.00| 2014| 5|
｜ Canada |Channel Partners| 60432.00| 2014| 6|
｜ Canada |Enterprise | 395750.00| 2014| 6|
｜ Canada |Government | 1886610.00| 2014| 6|
｜ Canada |Midmarket | 85320.00| 2014| 6|
｜ Canada |Small Business | 656400.00| 2014| 6|
｜ France |Channel Partners| 45624.00| 2014| 6|
｜ France |Enterprise | 196750.00| 2014| 6|
｜ France |Government | 1139820.00| 2014| 6|
｜ France |Midmarket | 65340.00| 2014| 6|
｜ France |Small Business | 268800.00| 2014| 6|
｜ Germany |Channel Partners| 37080.00| 2014| 6|
｜ Germany |Enterprise | 392500.00| 2014| 6|
｜ Germany |Government | 791572.00| 2014| 6|
｜ Germany |Midmarket | 26640.00| 2014| 6|
｜ Germany |Small Business | 413400.00| 2014| 6|
｜ Mexico |Channel Partners| 14496.00| 2014| 6|
｜ Mexico |Enterprise | 165500.00| 2014| 6|
｜ Mexico |Government | 682692.00| 2014| 6|
｜ Mexico |Midmarket | 74100.00| 2014| 6|
｜ Mexico |Small Business | 1476000.00| 2014| 6|
｜ United States of America|Channel Partners| 27408.00| 2014| 6|
｜ United States of America|Enterprise | 181750.00| 2014| 6|
｜ United States of America|Government | 513378.00| 2014| 6|
｜ United States of America|Midmarket | 77010.00| 2014| 6|
｜ United States of America|Small Business | 594600.00| 2014| 6|
｜ Canada |Channel Partners| 48312.00| 2014| 7|
｜ Canada |Enterprise | 333187.50| 2014| 7|
｜ Canada |Government | 759618.00| 2014| 7|
｜ Canada |Midmarket | 24457.50| 2014| 7|
｜ Canada |Small Business | 1138050.00| 2014| 7|
｜ France |Channel Partners| 29106.00| 2014| 7|
｜ France |Enterprise | 373500.00| 2014| 7|
｜ France |Government | 275110.50| 2014| 7|
｜ France |Midmarket | 58117.50| 2014| 7|
｜ France |Small Business | 497700.00| 2014| 7|
｜ Germany |Channel Partners| 4404.00| 2014| 7|
｜ Germany |Enterprise | 439125.00| 2014| 7|
｜ Germany |Government | 482152.00| 2014| 7|
｜ Germany |Midmarket | 7380.00| 2014| 7|
｜ Germany |Small Business | 843300.00| 2014| 7|
｜ Mexico |Channel Partners| 6852.00| 2014| 7|
｜ Mexico |Enterprise | 227875.00| 2014| 7|
｜ Mexico |Government | 545131.00| 2014| 7|
｜ Mexico |Midmarket | 9615.00| 2014| 7|
｜ Mexico |Small Business | 240300.00| 2014| 7|
｜ United States of America|Channel Partners| 16434.00| 2014| 7|
｜ United States of America|Enterprise | 453375.00| 2014| 7|
｜ United States of America|Government | 1233933.00| 2014| 7|
｜ United States of America|Midmarket | 47992.50| 2014| 7|
｜ United States of America|Small Business | 738000.00| 2014| 7|
｜ Canada |Channel Partners| 22608.00| 2014| 8|
｜ Canada |Enterprise | 115375.00| 2014| 8|
｜ Canada |Government | 253890.00| 2014| 8|
｜ Canada |Midmarket | 38385.00| 2014| 8|
｜ Canada |Small Business | 562200.00| 2014| 8|
｜ France |Channel Partners| 29700.00| 2014| 8|
｜ France |Enterprise | 146750.00| 2014| 8|
｜ France |Government | 183802.00| 2014| 8|
｜ France |Midmarket | 31515.00| 2014| 8|
｜ France |Small Business | 468600.00| 2014| 8|
｜ Germany |Channel Partners| 30888.00| 2014| 8|
｜ Germany |Enterprise | 345875.00| 2014| 8|
｜ Germany |Government | 178906.00| 2014| 8|
｜ Germany |Midmarket | 26145.00| 2014| 8|
｜ Germany |Small Business | 557700.00| 2014| 8|
｜ Mexico |Channel Partners| 13476.00| 2014| 8|
｜ Mexico |Enterprise | 192500.00| 2014| 8|
｜ Mexico |Government | 593061.00| 2014| 8|
｜ Mexico |Midmarket | 29760.00| 2014| 8|
｜ Mexico |Small Business | 287400.00| 2014| 8|
｜ United States of America|Channel Partners| 25692.00| 2014| 8|
｜ United States of America|Enterprise | 352625.00| 2014| 8|
｜ United States of America|Government | 1043836.00| 2014| 8|
｜ United States of America|Midmarket | 32970.00| 2014| 8|
｜ United States of America|Small Business | 762300.00| 2014| 8|
｜ Canada |Channel Partners| 17340.00| 2014| 9|
｜ Canada |Enterprise | 70875.00| 2014| 9|
｜ Canada |Government | 291646.00| 2014| 9|
｜ Canada |Midmarket | 3270.00| 2014| 9|
｜ Canada |Small Business | 640200.00| 2014| 9|
｜ France |Channel Partners| 32052.00| 2014| 9|
｜ France |Enterprise | 82875.00| 2014| 9|
｜ France |Government | 1041108.00| 2014| 9|
｜ France |Midmarket | 39300.00| 2014| 9|
｜ France |Small Business | 645300.00| 2014| 9|
｜ Germany |Channel Partners| 18960.00| 2014| 9|
｜ Germany |Enterprise | 260875.00| 2014| 9|
｜ Germany |Government | 721152.00| 2014| 9|
｜ Germany |Midmarket | 11205.00| 2014| 9|
｜ Germany |Small Business | 295800.00| 2014| 9|
｜ Mexico |Channel Partners| 6744.00| 2014| 9|
｜ Mexico |Enterprise | 263750.00| 2014| 9|
｜ Mexico |Government | 664450.00| 2014| 9|
｜ Mexico |Midmarket | 37080.00| 2014| 9|
｜ Mexico |Small Business | 129600.00| 2014| 9|
｜ United States of America|Channel Partners| 23364.00| 2014| 9|
｜ United States of America|Enterprise | 199500.00| 2014| 9|
｜ United States of America|Government | 762266.00| 2014| 9|
｜ United States of America|Midmarket | 26505.00| 2014| 9|
｜ United States of America|Small Business | 560100.00| 2014| 9|
｜ Canada |Channel Partners| 31080.00| 2014| 10|
｜ Canada |Enterprise | 502250.00| 2014| 10|
｜ Canada |Government | 976536.00| 2014| 10|
｜ Canada |Midmarket | 46950.00| 2014| 10|
｜ Canada |Small Business | 897600.00| 2014| 10|
｜ France |Channel Partners| 33432.00| 2014| 10|
｜ France |Enterprise | 610250.00| 2014| 10|
｜ France |Government | 1627174.00| 2014| 10|
｜ France |Midmarket | 36810.00| 2014| 10|
｜ France |Small Business | 1308600.00| 2014| 10|
｜ Germany |Channel Partners| 11328.00| 2014| 10|
｜ Germany |Enterprise | 271250.00| 2014| 10|
｜ Germany |Government | 2028580.00| 2014| 10|
｜ Germany |Midmarket | 35250.00| 2014| 10|
｜ Germany |Small Business | 1212600.00| 2014| 10|
｜ Mexico |Channel Partners| 9840.00| 2014| 10|
｜ Mexico |Enterprise | 539000.00| 2014| 10|
｜ Mexico |Government | 1051746.00| 2014| 10|
｜ Mexico |Midmarket | 60930.00| 2014| 10|
｜ Mexico |Small Business | 327600.00| 2014| 10|
｜ United States of America|Channel Partners| 69936.00| 2014| 10|
｜ United States of America|Enterprise | 215250.00| 2014| 10|
｜ United States of America|Government | 768842.00| 2014| 10|
｜ United States of America|Midmarket | 34590.00| 2014| 10|
｜ United States of America|Small Business | 606000.00| 2014| 10|
｜ Canada |Channel Partners| 27852.00| 2014| 11|
｜ Canada |Enterprise | 316125.00| 2014| 11|
｜ Canada |Government | 280016.00| 2014| 11|
｜ Canada |Midmarket | 40335.00| 2014| 11|
｜ Canada |Small Business | 409800.00| 2014| 11|
｜ France |Channel Partners| 28104.00| 2014| 11|
｜ France |Enterprise | 218000.00| 2014| 11|
｜ France |Government | 581193.00| 2014| 11|
｜ France |Midmarket | 7350.00| 2014| 11|
｜ France |Small Business | 397200.00| 2014| 11|
｜ Germany |Channel Partners| 28104.00| 2014| 11|
｜ Germany |Enterprise | 69000.00| 2014| 11|
｜ Germany |Government | 174005.00| 2014| 11|
｜ Germany |Midmarket | 22695.00| 2014| 11|
｜ Germany |Small Business | 407700.00| 2014| 11|
｜ Mexico |Channel Partners| 8280.00| 2014| 11|
｜ Mexico |Enterprise | 109625.00| 2014| 11|
｜ Mexico |Government | 462161.00| 2014| 11|
｜ Mexico |Midmarket | 41865.00| 2014| 11|
｜ Mexico |Small Business | 645000.00| 2014| 11|
｜ United States of America|Channel Partners| 32676.00| 2014| 11|
｜ United States of America|Enterprise | 298375.00| 2014| 11|
｜ United States of America|Government | 440499.00| 2014| 11|
｜ United States of America|Midmarket | 30450.00| 2014| 11|
｜ United States of America|Small Business | 871500.00| 2014| 11|
｜ Canada |Channel Partners| 58344.00| 2014| 12|
｜ Canada |Enterprise | 682250.00| 2014| 12|
｜ Canada |Government | 2091228.00| 2014| 12|
｜ Canada |Midmarket | 69000.00| 2014| 12|
｜ Canada |Small Business | 1149600.00| 2014| 12|
｜ France |Channel Partners| 25320.00| 2014| 12|
｜ France |Enterprise | 321750.00| 2014| 12|
｜ France |Government | 1555038.00| 2014| 12|
｜ France |Midmarket | 62160.00| 2014| 12|
｜ France |Small Business | 511800.00| 2014| 12|
｜ Germany |Channel Partners| 24312.00| 2014| 12|
｜ Germany |Enterprise | 426500.00| 2014| 12|
｜ Germany |Government | 1124260.00| 2014| 12|
｜ Germany |Midmarket | 21330.00| 2014| 12|
｜ Germany |Small Business | 750000.00| 2014| 12|
｜ Mexico |Channel Partners| 26016.00| 2014| 12|
｜ Mexico |Enterprise | 284500.00| 2014| 12|
｜ Mexico |Government | 1010014.00| 2014| 12|
｜ Mexico |Midmarket | 64710.00| 2014| 12|
｜ Mexico |Small Business | 381000.00| 2014| 12|
｜ United States of America|Channel Partners| 21936.00| 2014| 12|
｜ United States of America|Enterprise | 699250.00| 2014| 12|
｜ United States of America|Government | 306300.00| 2014| 12|
｜ United States of America|Midmarket | 18450.00| 2014| 12|
｜ United States of America|Small Business | 823200.00| 2014| 12|
</details>
